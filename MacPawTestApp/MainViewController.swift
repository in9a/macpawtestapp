//
//  ViewController.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 16.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

struct Constants {
    static let segueShow = "show"
    
    enum Data :Int {
        case deleted
        case error
        case cantBeDeleted
    }
}

class MainViewController: NSViewController {
    
    var isReceivingDrag = false {
        didSet {
            if isReceivingDrag{
                NSColor.selectedControlColor.set()
                
                let path = NSBezierPath(rect:tableView.bounds)
                path.lineWidth = 20
                path.stroke()
            }
        }
    }
    
    @IBOutlet weak var tableView: NSTableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.columnAutoresizingStyle = .firstColumnOnlyAutoresizingStyle
            tableView.selectionHighlightStyle = .regular
            tableView.backgroundColor = NSColor.testBackgroundColor()
            
            let descriptorName = NSSortDescriptor(key: Directory.OrderType.name.rawValue, ascending: true)
            let descriptorDate = NSSortDescriptor(key: Directory.OrderType.date.rawValue, ascending: true)
            let descriptorSize = NSSortDescriptor(key: Directory.OrderType.size.rawValue, ascending: true)
            
            tableView.tableColumns[0].sortDescriptorPrototype = descriptorName;
            tableView.tableColumns[1].sortDescriptorPrototype = descriptorDate;
            tableView.tableColumns[2].sortDescriptorPrototype = descriptorSize;
            
    
            tableView.tableColumns[0].title = NSLocalizedString("name", comment: "")
            tableView.tableColumns[1].title = NSLocalizedString("date", comment: "")
            tableView.tableColumns[2].title = NSLocalizedString("name", comment: "size")
            
            var acceptableTypes: Set<String> { return [NSURLPboardType] }
            tableView.register(forDraggedTypes: Array(acceptableTypes))
        }
    }
    @IBOutlet weak var deleteButton: NSButtonCell!{
        didSet{
            deleteButton.title = NSLocalizedString("delete", comment: "")
            deleteButton.isEnabled = false
        }
    }
    @IBOutlet weak var importButton: NSButton!{
        didSet{
            
            importButton.title = NSLocalizedString("Import", comment: "")
            
        }
    }
    @IBOutlet weak var addButton: NSButton!{
        didSet{
            
            addButton.title = NSLocalizedString("Add", comment: "")
            addButton.isEnabled = false
        }
    }
    @IBOutlet weak var clearButton: NSButton!{
        didSet{
            
            clearButton.title = NSLocalizedString("Clear", comment: "")
            clearButton.isEnabled = false
        }
    }
    @IBOutlet weak var folderSelectButton: NSButton!{
        didSet{
            folderSelectButton.state = NSOnState
            folderSelectButton.tag = RadioTypes.folder.hashValue
            
            folderSelectButton.title  = NSLocalizedString("asFolder", comment: "")        }
    }
    @IBOutlet weak var enumerateSelectButton: NSButton!{
        didSet{
            enumerateSelectButton.tag = RadioTypes.enumerate.hashValue
            
            enumerateSelectButton.title = NSLocalizedString("content", comment: "")
        }
    }
    
    lazy var directory:Directory = {
        let dir = Directory(withDelegate: self)
        return dir
    }()
    
    var items:[Metadata]?{
        didSet{
            addButton.isEnabled = items?.count != 0
            clearButton.isEnabled = items?.count != 0
            deleteButton.isEnabled = items!.filter { (item) -> Bool in return item.isChoosen  }.count != 0
        }
    }
    
    //Sorting
    var sortBy = Directory.OrderType.name
    var ascending = true
    
    // Other
    var asFolder = true
    var isDeleted = true
    var isImport = true
    fileprivate enum RadioTypes {
        case folder, enumerate
    }
    
    //Progress
    var isProgress = false {
        didSet{
            if isProgress {
                showProgressView()
            } else {
                hideProgress()
            }
        }
    }
    lazy var progressView:MainProgressView = {
        
        let viewProgress = MainProgressView.init(frame: NSRect(x: 20, y: 200, width: 200, height: 80))
        viewProgress.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(viewProgress)
        viewProgress.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        viewProgress.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        viewProgress.widthAnchor.constraint(equalToConstant: 200).isActive = true
        viewProgress.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        viewProgress.isHidden = true
        
        return viewProgress
    }()
    var pulse:PulsingLayer?
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override var representedObject: Any? {
        didSet {
            if let urls = representedObject as? [URL]{
                
                if isImport{
                    directory.importFiles(withURL: urls, asFolder: asFolder)
                } else {
                    directory.addFiles(withURL: urls, asFolder: asFolder)
                }
            }
        }
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        isProgress = false
    }
    
    // MARK: Open Panel
    @IBAction func openPanelAction(_ sender: AnyObject) {
    
        isImport = true
        
        openPanel()
    }

    @IBAction func addAction(_ sender: AnyObject) {
        isImport = false
        
        openPanel()
    }
    
    @IBAction func clearAction(_ sender: AnyObject) {
        directory.clearAll()
    }
    
    private func openPanel(){
        let openPanel = NSOpenPanel()
        openPanel.showsHiddenFiles      = false
        openPanel.canChooseFiles        = true
        openPanel.canChooseDirectories  = true
        openPanel.allowsMultipleSelection = true
        
        openPanel.begin {(response) in
            guard response == NSFileHandlingPanelOKButton else { return }
            
            self.representedObject = openPanel.urls
        }
    }
    
    // MARK: Progress
    func showProgressView(){
        self.hideProgress()
        
        progressView.isHidden = false
        
    
        self.pulse = PulsingLayer(radius: 110, position: CGPoint.init(
            x: self.progressView.frame.origin.x + (self.progressView.frame.size.width / 2),
            y: self.progressView.frame.origin.y + (self.progressView.frame.size.height / 2)))
        
        self.pulse!.backgroundColor = NSColor.white.cgColor
        self.pulse!.animationDuration = 4
        
        self.view.layer?.insertSublayer(self.pulse!, below:self.progressView.layer)
        
    }
    
    func hideProgress(){
        progressView.isHidden = true
        
        if let p = pulse{
            p.removeFromSuperlayer()
        }
        
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        
        pulse?.position = CGPoint.init(
            x: self.progressView.frame.origin.x + (self.progressView.frame.size.width / 2),
            y: self.progressView.frame.origin.y + (self.progressView.frame.size.height / 2))
    }
    
    // MARK: Actions
    @IBAction func radioAction(_ sender: NSButton) {
        // TODO: ANTON!!!!!!!!!!!
        if sender.tag == RadioTypes.folder.hashValue{
            asFolder = true
            showProgressView()
        }
        else if sender.tag == RadioTypes.enumerate.hashValue{
            asFolder = false
            hideProgress()
        }
    }
    
    @IBAction func deleteAction(_ sender: NSButtonCell) {
        showAlert()
    }
    
    private func showAlert(){
        
        let alert = NSAlert()
        
        alert.messageText = NSLocalizedString("sure", comment: "")
        alert.informativeText = NSLocalizedString("cannotBe", comment: "")
        alert.alertStyle = .warning
        
        alert.addButton(withTitle: NSLocalizedString("delete", comment: ""))
        alert.addButton(withTitle: NSLocalizedString("trash", comment: ""))
        alert.addButton(withTitle: NSLocalizedString("cancel", comment: ""))
        
        alert.beginSheetModal(for: self.view.window!) { (response) in
            switch response {
            case NSAlertFirstButtonReturn:
                self.directory.deleteFiles()
            case NSAlertSecondButtonReturn:
                self.directory.deleteToTrash()
            default:
                break
            }
        }
    }
    
    
    fileprivate func update(){
        items = directory.sortOrderdBy(orderedBy: sortBy, ascending: ascending)
        tableView.reloadData()
    }
    
    // MARK: Segue
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueShow {
            if let vc = segue.destinationController as? InfoViewController,let item = sender as? [Int: [Metadata]]{
                vc.items = item
                vc.isDeleted = isDeleted
            }
        }
    }
}

extension MainViewController: DirectoryDelegate{
    func startProgress(){
        isProgress = true
    }
    
    func update(withFiles files: [Metadata], withDeleted deletedFiles: [Metadata], cantBeDeletedFiles cantFiles: [Metadata], andError errorFiles: [Metadata], deleted:Bool) {
        update()
        
        isProgress = false
        isDeleted = deleted
        
        let dictionary:[Int : [Metadata]] = [Constants.Data.deleted.hashValue : deletedFiles,
                                             Constants.Data.cantBeDeleted.hashValue : cantFiles,
                                             Constants.Data.error.hashValue : errorFiles]
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { 
            self.performSegue(withIdentifier: Constants.segueShow, sender: dictionary)
        }
        
    }
    
    func updateUI(withFiles files: [Metadata]) {
        isProgress = false
        
        update()
    }
}

extension MainViewController:NSTableViewDelegate, NSTableViewDataSource{
    // MARK: TableView
    func numberOfRows(in tableView: NSTableView) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        var text:String = ""
        var cellIdentifier: String = ""
        
        guard let item = items?[row] else {
            return nil
        }
        
        if tableColumn == tableView.tableColumns[0] {
            
            let cell = tableView.make(withIdentifier: "MainTableCellView", owner: nil) as! MainTableCellView
            cell.setUp(withItem: item, index: row)
            cell.delegate = self
            
            return cell
        }
        else if tableColumn == tableView.tableColumns[1] {
            cellIdentifier = "DateID"
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            formatter.dateStyle = .short
            text = formatter.string(from: item.date)
        }
        else if tableColumn == tableView.tableColumns[2] {
            cellIdentifier = "SizeID"
            
            let sizeFormatter = ByteCountFormatter()
            text = item.isFolder ? "--" : sizeFormatter.string(fromByteCount: item.size)
        }
        
        if let cell = tableView.make(withIdentifier: cellIdentifier, owner: nil) as? NSTableCellView {
            cell.textField?.stringValue = text
            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 50
        
        
    }
    
    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        
        guard let sortDescriptor = tableView.sortDescriptors.first else {
            return
        }
        
        if let order = Directory.OrderType(rawValue: sortDescriptor.key!){
            sortBy = order
            ascending = sortDescriptor.ascending
            update()
        }
    }
    
    //MARK: DRAG AND DROP
    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableViewDropOperation) -> Bool {
        
        print(info)
        print(row)
        print(dropOperation)
        
        let pasteBoard = info.draggingPasteboard()
       
        
        if let urls = pasteBoard.readObjects(forClasses: [NSURL.self], options:nil) as? [URL], urls.count > 0 {
            
            directory.addFiles(withURL: urls, asFolder: asFolder)
            
            return true
        }
        
        return true
    }
    
    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableViewDropOperation) -> NSDragOperation{
        
        isReceivingDrag = true
        
        return .copy
    }
}

extension MainViewController: CellDelegate{
    func checBoxValueChanged(_ checkBox:NSButton){
        switch checkBox.state {
        case NSOnState:
            directory.updateFiles(withIndex: checkBox.tag, andState: true)
        default:
            directory.updateFiles(withIndex: checkBox.tag, andState: false)
        }
    }
}










