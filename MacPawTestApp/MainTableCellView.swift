//
//  MainTableCellView.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 16.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

protocol CellDelegate:class {
    func checBoxValueChanged(_ checkBox:NSButton)
}

class MainTableCellView: BaseCell {

    weak var delegate: CellDelegate?
    
    @IBOutlet weak var checkBox: NSButton!
    @IBOutlet weak var mainImage: NSImageView!
    @IBOutlet weak var descriptionLabel: NSTextField!{
        didSet{
            descriptionLabel.isHidden = true
            descriptionLabel.font = NSFont.systemFont(ofSize: 12)
            descriptionLabel.textColor = NSColor.red
            descriptionLabel.sizeToFit()
        }
    }
    
    var item:Metadata? {
        didSet{
            mainImage.image = item!.icon
            textField?.stringValue = item!.name
            descriptionLabel.stringValue = item!.url.standardizedFileURL.absoluteString
        }
    }
    
    func setUp(withItem item:Metadata, index:Int){
        self.item = item
        self.checkBox.tag = index
        switch item.isChoosen {
        case true:
            self.checkBox.state = NSOnState
        default:
            self.checkBox.state = NSOffState
        }
    }
   
    @IBAction func checkAction(_ sender: NSButton) {
        self.delegate?.checBoxValueChanged(sender)
    }
    
    override func mouseEntered(with event: NSEvent) {
        self.descriptionLabel.isHidden = false
    }
    
    override func mouseExited(with event: NSEvent) {
        self.descriptionLabel.isHidden = true
    }
    
}










