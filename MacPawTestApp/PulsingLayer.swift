//
//  PulsingLayer.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 18.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class PulsingLayer: CALayer {

    var initialPulseScale:Float = 0
    var animationGroup = CAAnimationGroup()
    var nextPulseTime:TimeInterval = 0.5
    var animationDuration:TimeInterval = 1
    var radius:CGFloat = 200
    var numberOfPulses = Float.infinity
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    init(radius:CGFloat, position:CGPoint) {
        super.init()
        
        self.backgroundColor = NSColor.black.cgColor
//        self.contentsScale = NSScreen.main()?.backingScaleFactor
        self.opacity = 0
        self.radius = radius
        self.position = position
        
        self.bounds = CGRect(x: 0, y: 0, width: radius * 2, height: radius * 2)
        self.cornerRadius = radius
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                self.setupGroup()
            
            DispatchQueue.main.async {
                self.add(self.animationGroup, forKey: "pulsing")
            }
        }
    }
    
    private func createScaleAnimator() -> CABasicAnimation{
        let scaleAnimator = CABasicAnimation(keyPath: "transfor.scale.xy")
        scaleAnimator.fromValue = NSNumber(value: initialPulseScale)
        scaleAnimator.toValue = NSNumber(value: 1)
        scaleAnimator.duration = animationDuration
        
        return scaleAnimator
    }
    
    private func createOpacityAnimation() -> CAKeyframeAnimation{
        let opacityAnimator = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimator.values = [0.2,0.6,0.2]
        opacityAnimator.keyTimes = [0, 0.4, 1]
        
        return opacityAnimator
    }
    
    private func setupGroup(){
        
        self.animationGroup = CAAnimationGroup()
        animationGroup.duration = animationDuration + nextPulseTime
        animationGroup.repeatCount = numberOfPulses
        
        let curve = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        animationGroup.timingFunction = curve

        animationGroup.animations = [createScaleAnimator(), createOpacityAnimation()]
    }
    
    func stop(){
        numberOfPulses = 0
    }
    
}
