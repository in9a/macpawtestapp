//
//  BaseCell.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class BaseCell: NSTableCellView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.wantsLayer = true
//        self.layer?.backgroundColor = NSColor.testBackgroundColor().cgColor
        
        self.textField?.textColor = NSColor.white
    }
    
    var trackingRect: NSTrackingRectTag?
    
    override func viewDidMoveToWindow() {
        trackingRect = self.addTrackingRect(self.frame, owner: self, userData: nil, assumeInside: false)
    }
}

