//
//  Metadata.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

public struct Metadata{
    
    let name:String
    let date:Date
    let size:Int64
    let icon:NSImage
    let isFolder:Bool
    let url:URL
    var isChoosen = true
    
    init(fileURL:URL, name:String, date:Date, size:Int64, icon:NSImage, isFolder:Bool ) {
        self.name  = name
        self.date = date
        self.size = size
        self.icon = icon
        self.isFolder = isFolder
        url = fileURL
    }
}

//MARK: - Sorting


func sortMetadata(lhsIsFolder:Bool, rhsIsFolder:Bool,  ascending:Bool , attributeComparation:Bool ) -> Bool
{
    if( lhsIsFolder && !rhsIsFolder) {
        return ascending ? true : false
    }
    else if ( !lhsIsFolder && rhsIsFolder ) {
        return ascending ? false : true
    }
    return attributeComparation
}


func itemComparator<T:Comparable>( lhs:T, rhs:T, ascending:Bool ) -> Bool {
    return ascending ? (lhs < rhs) : (lhs > rhs)
}


//Dat
public func ==(lhs: Date, rhs: Date) -> Bool {
    if lhs.compare(rhs) == .orderedSame {
        return true
    }
    return false
}

public func <(lhs: Date, rhs: Date) -> Bool {
    if lhs.compare(rhs) == .orderedAscending {
        return true
    }
    return false
}
