//
//  InfoViewController.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class InfoViewController: NSViewController {
    
    var items:[Int : [Metadata]]?
    var isDeleted = true
    let backView :RoundView = {
        let view = RoundView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.radius = 20
        
        return view
    }()
    let successImage:NSImageView = {
        let imageView = NSImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "iconSuccess")
        return imageView
    }()
    let textLabel:NSTextField = {
        let textLabel = NSTextField()
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.isBezeled = false
        textLabel.drawsBackground = false
        textLabel.isEditable = false
        textLabel.isSelectable = false
        textLabel.alignment = .center
        textLabel.font = NSFont.boldSystemFont(ofSize: 18)
        textLabel.textColor = NSColor.red
        
        return textLabel
    }()
    lazy var dismissButton:NSButton = {
        let button = NSButton()
        button.bezelStyle = .rounded
        button.translatesAutoresizingMaskIntoConstraints = false
        button.target = self
        button.action = #selector(dismissController)
        button.title = "Ok"
        
        return button
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferredContentSize = CGSize(width: 300, height: 500)
        
        setupViews()
        
        updateValues()
    }
    
    func dismissController(){
        self.dismiss(nil)
    }
    
    private func setupViews(){
        
        self.view.addSubview(backView)
        backView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: -8).isActive = true
        backView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 8).isActive = true
        backView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: -8).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 8).isActive = true
        
        self.view.addSubview(successImage)
        self.view.addConstraint(NSLayoutConstraint(
            item: successImage,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .centerX,
            multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(
            item: successImage,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .centerY,
            multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(
            item: successImage,
            attribute: .width,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .width,
            multiplier: 0.5, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(
            item: successImage,
            attribute: .height,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .height,
            multiplier: 0.5, constant: 0))
        
        self.view.addSubview(textLabel)
        
        self.view.addConstraintsWithFormat(format:"H:|-20-[v0]-20-|", views: textLabel)
        textLabel.topAnchor.constraint(equalTo: successImage.bottomAnchor, constant: 20).isActive = true
        
        
        self.view.addSubview(dismissButton)
        dismissButton.bottomAnchor.constraint(equalTo: successImage.topAnchor, constant: -50).isActive = true
        dismissButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        dismissButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        dismissButton.centerXAnchor.constraint(equalTo: successImage.centerXAnchor).isActive = true
    }
    
    private func updateValues(){
        
        if let item = self.items{
            
            if isDeleted == false{
                textLabel.stringValue = NSLocalizedString("removedTo", comment: "")
                return
            }
            
            var size:Int64 = 0
            var count = 0
            var countText = ""
            if let deleted = item[Constants.Data.deleted.hashValue]{
                for item in deleted {
                    size += item.size
                }
                count = deleted.count
            }
            if size != 0 {
                countText = "(\(size) Kb)"
            }
            
            var cantText = ""
            if let cantBeDeleted = item[Constants.Data.cantBeDeleted.hashValue], cantBeDeleted.count > 0{
                cantText = "\n \(cantBeDeleted.count) - \(NSLocalizedString("oops", comment: ""))"
            }
            
            var errorIn = ""
            if let errorItems = item[Constants.Data.error.hashValue], errorItems.count > 0{
                errorIn = "\n \(NSLocalizedString("error", comment: "")) \(NSLocalizedString("in", comment: "")) \(errorItems.count) \(NSLocalizedString("files", comment: ""))"
            }
            
            textLabel.stringValue = "\(NSLocalizedString("haveDeleted", comment: "")) \(count) \(NSLocalizedString("obj", comment: "")) \(countText) \(cantText) \(errorIn)"
        }
        
        
        
    }
}
