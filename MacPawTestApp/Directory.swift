//
//  Directory.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 16.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

protocol DirectoryDelegate:class {
    func updateUI(withFiles files:[Metadata])
    
    func update(withFiles files:[Metadata],
                withDeleted deletedFiles:[Metadata],
                cantBeDeletedFiles cantFiles:[Metadata],
                andError errorFiles:[Metadata],
                deleted:Bool)
    
    func startProgress()
}

public class Directory  {
    
    weak var delegate:DirectoryDelegate?
    
    private var files = [Metadata]()
    
    var importAsFolder = true
    
    private let requiredAttributes = [URLResourceKey.localizedNameKey,URLResourceKey.effectiveIconKey, URLResourceKey.typeIdentifierKey,URLResourceKey.creationDateKey,URLResourceKey.fileSizeKey,URLResourceKey.isDirectoryKey,URLResourceKey.isPackageKey]
    
    private let optionsFileManager:FileManager.DirectoryEnumerationOptions = [.skipsHiddenFiles, .skipsPackageDescendants, .skipsSubdirectoryDescendants]
    
    //MARK: Public API
    init(withDelegate delegate: DirectoryDelegate) {
        self.delegate = delegate
    }
    
    public func importFiles(withURL urls:[URL], asFolder:Bool){
        files.removeAll()
        
        addFiles(withURL: urls, asFolder: asFolder)
    }
    
    public func addFiles(withURL urls:[URL], asFolder:Bool){
        importAsFolder = asFolder
        
        for folderURL in urls {
            
            var directory: ObjCBool = ObjCBool(false)
            let exists = FileManager.default.fileExists(atPath: String(describing: folderURL.path), isDirectory: &directory)
            
            if !exists { return }
            
            
            switch directory.boolValue
            {
            case false:
                enumerate(inUrl: folderURL)
            default:
                
                if asFolder{
                    enumerate(inUrl: folderURL)
                }
                else {
                    if let enumarate = FileManager.default.enumerator( at: folderURL, includingPropertiesForKeys: requiredAttributes,options: optionsFileManager, errorHandler: nil){
                        
                        for url in enumarate {
                            guard let currentURL = url as? URL else { return }
                            
                            enumerate(inUrl: currentURL)
                        }
                    }
                }
            }
        }
        
        if files.count > 0{
            
            self.delegate?.updateUI(withFiles: files)
        }
    }
    
    public func clearAll(){
        files.removeAll()
        
        DispatchQueue.main.async {[weak self] in
            
            guard let strongSelf = self else {return}
            
            strongSelf.delegate?.updateUI(withFiles: strongSelf.files)
        }
    }
    
    // MARK: Delete
    public func deleteFiles(){
         
        //Showing progress
        DispatchQueue.main.async {[weak self] in
            
            guard let strongSelf = self else {return}
            
            strongSelf.delegate?.startProgress()
        }
        
        
        //Delete files on background thread
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async { [weak self] in
            
            guard let strongSelf = self else {return}
            
            var array = [Metadata]()
            var cantBeDeleted = [Metadata]()
            var errorInDeliting = [Metadata]()
            var deleted = [Metadata]()
            
            for file in strongSelf.files{
                if !file.isChoosen {
                    array.append(file)
                }
                else {
                    if FileManager.default.isDeletableFile(atPath: file.url.path){
                        do {
                            try FileManager.default.removeItem(atPath: file.url.path)
                            deleted.append(file)
                        } catch let error {
                            print("error \(error)")
                            errorInDeliting.append(file)
                        }
                    } else {
                        cantBeDeleted.append(file)
                    }
                }
            }
            
            strongSelf.files = array
            
            // get on main queue and update UI
            DispatchQueue.main.async {[weak self] in
                
                guard let strongSelf = self else {return}

                
                strongSelf.delegate?.update(withFiles:strongSelf.files,
                                      withDeleted: deleted,
                                      cantBeDeletedFiles: cantBeDeleted,
                                      andError: errorInDeliting,
                                      deleted:true)
            }
        }
        
        
        
    
        
        
    }
    
    public func deleteToTrash(){
        
        DispatchQueue.main.async {[weak self] in
            
            guard let strongSelf = self else {return}
        
            strongSelf.delegate?.startProgress()
        }
        
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async { [weak self] in
            
            guard let strongSelf = self else {return}
        
            var array = [Metadata]()
            var cantBeDeleted = [Metadata]()
            var errorInDeliting = [Metadata]()
            var deleted = [Metadata]()
            
            for file in strongSelf.files{
                if !file.isChoosen {
                    array.append(file)
                }
                else {
                    if FileManager.default.isDeletableFile(atPath: file.url.path){
                        do {
                            try FileManager.default.trashItem(at: file.url, resultingItemURL: nil)
                            deleted.append(file)
                        } catch let error {
                            print("error \(error)")
                            errorInDeliting.append(file)
                        }
                    } else {
                        cantBeDeleted.append(file)
                    }
                }
            }
            
            strongSelf.files = array
            
            DispatchQueue.main.async {[weak self] in
                
                guard let strongSelf = self else {return}
                
                strongSelf.delegate?.update(withFiles:strongSelf.files,
                                            withDeleted: deleted,
                                            cantBeDeletedFiles: cantBeDeleted,
                                            andError: errorInDeliting,
                                            deleted:false)
            }
        }
    }
    
    // MARK: Update
    public func updateFiles(withIndex index:Int, andState state: Bool){
        
        if index >= files.count {return}
        
        files[index].isChoosen = state
        
        print(files)
    }
    
    // MARK: Other
    private func enumerate(inUrl folderUrl:URL){
        
        let contains = files.filter { (item) -> Bool in
            return item.url == folderUrl
            }
        if contains.count > 0 {
            return
        }
        
        do {
            let properties = try (folderUrl as NSURL).resourceValues(forKeys: requiredAttributes)
            
            files.append(Metadata(fileURL: folderUrl,
                                  name: properties[URLResourceKey.localizedNameKey] as? String ?? "",
                                  date: properties[URLResourceKey.creationDateKey] as? Date ?? Date.distantPast,
                                  size: (properties[URLResourceKey.fileSizeKey] as? NSNumber)?.int64Value ?? 0,
                                  icon: properties[URLResourceKey.effectiveIconKey] as? NSImage  ?? NSImage(),
                                  isFolder: (properties[URLResourceKey.isDirectoryKey] as? NSNumber)?.boolValue ?? false))
        } catch let error {
            print(error)
        }
    }
    
    //MARK: Sort Order
    public enum OrderType : String {
        case name
        case date
        case size
    }
    
    func sortOrderdBy(orderedBy:OrderType, ascending:Bool) -> [Metadata]{
        
        switch orderedBy
        {
        case .name:
            files = files.sorted(by: { return sortMetadata(
                lhsIsFolder: $0.isFolder,
                rhsIsFolder: $1.isFolder,
                ascending: ascending,
                attributeComparation: itemComparator(lhs:$0.name, rhs: $1.name, ascending:ascending))
            })
        case .size:
            files = files.sorted{ return sortMetadata(
                lhsIsFolder:$0.isFolder,
                rhsIsFolder: $0.isFolder,
                ascending:ascending,
                attributeComparation:itemComparator(lhs:$0.size, rhs: $1.size, ascending: ascending)) }
        case .date:
            files = files.sorted{ return sortMetadata(
                lhsIsFolder: $0.isFolder,
                rhsIsFolder: $0.isFolder,
                ascending:ascending,
                attributeComparation:itemComparator(lhs:$0.date, rhs: $1.date, ascending:ascending)) }
        }
        return files
    }
}

