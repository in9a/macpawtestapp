//
//  AppDelegate.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 16.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

