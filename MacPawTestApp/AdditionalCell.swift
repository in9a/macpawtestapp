//
//  AdditionalCell.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class AdditionalCell: BaseCell {

    override var textField: NSTextField?{
        didSet{
            textField?.alignment = .center
        }
    }    
}
