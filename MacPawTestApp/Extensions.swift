//
//  Extensions.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

extension NSColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> NSColor {
        return NSColor(calibratedRed: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
    
    static func testBackgroundColor() -> NSColor{
        //75 71 88
        return rgb(red: 75, green: 71, blue: 88, alpha: 1)
    }
}

extension NSView {
    func addConstraintsWithFormat(format: String, views: NSView...) {
        var viewsDictionary = [String: NSView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
