//
//  MainWindowController.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 17.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class MainWindowController: NSWindowController {
    override func windowDidLoad() {
        super.windowDidLoad()
    
        if let window = window, let screen = window.screen {
            
            let screenRect = screen.visibleFrame
            
            let offsetFromTopOfScreen: CGFloat = 100
           

            let width = screenRect.size.width * 0.5
            let height = screenRect.size.height * 0.5
            
            let newOriginY = screenRect.maxY - offsetFromTopOfScreen
            let newOriginX = (screenRect.maxX - width) / 2
            
            let rect = NSRect(x: newOriginX, y: newOriginY, width: width, height: height)
            
            window.setFrame(rect, display: true, animate: true)
            
            
            window.minSize = NSSize(width: 750, height: 250)
            
            window.title = "MacPaw Test App"
        }
    }
    
}
//
//enum Controllers:String {
//    case mainVC = "MainViewController"
//    case infoVC = "InfoViewController"
//}
