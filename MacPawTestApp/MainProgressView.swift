//
//  MainProgressView.swift
//  MacPawTestApp
//
//  Created by Anton Kharchevskyi on 18.10.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Cocoa

class MainProgressView: NSView {
    
    let titleLabel:NSTextField = {
        let textLabel = NSTextField()
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.isBezeled = false
        textLabel.drawsBackground = false
        textLabel.isEditable = false
        textLabel.isSelectable = false
        textLabel.alignment = .center
        textLabel.font = NSFont.boldSystemFont(ofSize: 14)
        textLabel.textColor = NSColor.darkGray
        
        return textLabel
    }()
    
    let progressBar:NSProgressIndicator = {
        let progress = NSProgressIndicator()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.isDisplayedWhenStopped = false
        progress.style = NSProgressIndicatorStyle.barStyle
        progress.startAnimation(nil)
        
        return progress
    }()
    
    let textLabel:NSTextField = {
        let textLabel = NSTextField()

        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.isBezeled = false
        textLabel.drawsBackground = false
        textLabel.isEditable = false
        textLabel.isSelectable = false
        textLabel.alignment = .center
        textLabel.font = NSFont.boldSystemFont(ofSize: 12)
        textLabel.textColor = NSColor.white
        
        return textLabel
    }()
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(){
    
        self.addSubview(progressBar)
        progressBar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        progressBar.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        progressBar.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        progressBar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        
        self.addSubview(titleLabel)
        titleLabel.bottomAnchor.constraint(equalTo: progressBar.topAnchor, constant: -8).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: progressBar.centerXAnchor, constant: 0).isActive = true
//        self.addConstraintsWithFormat(format:"H:|-20-[v0]-20-|", views: titleLabel)
        titleLabel.stringValue = NSLocalizedString("isDeleting", comment: "")
        
        self.addSubview(textLabel)
        textLabel.topAnchor.constraint(equalTo: progressBar.topAnchor, constant: 20).isActive = true
        textLabel.centerXAnchor.constraint(equalTo: progressBar.centerXAnchor, constant: 0).isActive = true
        self.addConstraintsWithFormat(format:"H:|-20-[v0]-20-|", views: textLabel)
        
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        let path = NSBezierPath(roundedRect: NSInsetRect(bounds, 0, 0), xRadius: 10, yRadius: 10)
//        NSColor.init(white: 0.8, alpha: 1.0).set()
        NSColor.white.set()
        path.fill()
    }
    
    func updateWithValues(_ countToDelete:Int, alreadyDeleted:Int){
        

        let text = "from"
        textLabel.stringValue = "\(alreadyDeleted) \(text) \(countToDelete)"
    }
    
    

}
